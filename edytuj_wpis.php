<?php
if (!isset($_GET['file'])) {
	header("Location: ./administracja.php");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administracja</title>
    </head>
    <body>
        <form method="post" action="zapisz_zmiany.php?file=<?php echo $_GET['file']; ?>">
            <textarea id="pole"  rows="20" cols="70" name="content">
                <?php echo file_get_contents($_GET['file']); ?>
            </textarea>
            <br/><br/>
            <input type="submit" value="Zapisz zmiany" name="zapisz" width="1000">
            <input type="button" name="powrot" value="Powrót" onclick="window.location.href='administracja.php'"/>
        </form>
    </body>
</html>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>CMS Projekt</title>
    </head>
    <body>
        <!-- Import wyglądu -->
        <link rel="stylesheet" type="text/css" href="naglowek.css" media="screen" /> 
        <link rel="stylesheet" type="text/css" href="notatki.css" media="screen" /> 
        <!-- Nagłówek z menu -->
        <h1>Blog programistyczny</h1>
        <table class="table_menu">
            <tr> 
                <td class="table_menu"><a href="logowanie.php"> Logowanie </td>
                <td class="table_menu"><a href="index.php"> Strona główna </td>
            </tr>
        </table>
        <br/><br/>
        <table class="tabela_notatki">
            <tr>
                <th>Data dodania</th>
                <th> Notka</th>
            </tr>
            <tbody>
                <?php
                    $pathToFiles = "./notes/";
                    $directory = opendir( $pathToFiles );
                    $count = 1;
                    $fileTable = [];
                    
                    while ( $file = readdir( $directory ) ) {
                        if ( is_file( $pathToFiles . $file ) ) {
                            $mtime = filemtime( $pathToFiles . $file );
                            $mtimestr = date( "j F Y, H:i:s", $mtime );
                            $temp['time'] = $mtimestr;
                            $temp['path'] = $pathToFiles . $file;
                            $temp['note'] = file_get_contents($pathToFiles . $file);
                            
                            $fileTable[$count] = $temp;
                            $count++;
                        }
                    }
                    
                    foreach ( $fileTable as $index => $entry ) {
                    $time = $entry['time'];
                    $note = $entry['note'];
                    $temp_note = substr($note,0,200);
                    $path = $entry['path'];
                    echo "<tr><td>$time";
                    echo "<td>$temp_note  ... <a href='notatka.php?akcja=wczytaj&file=$path'>(Czytaj dalej...)</a>";
                    }
                ?>
        </table>
    </body>
</html>

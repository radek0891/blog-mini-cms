<html>
    <head>
        <meta charset="UTF-8">
        <title>Administracja</title>
    </head>
    <body>
        <h1>Administracja</h1>
        <link rel="stylesheet" type="text/css" href="administracja.css" media="screen" /> 
        Istniejące notatki :
        <br/><br/>
        <table class="tabela_administracja">
            <thead>
            <tr>
                <th>Nazwa pliku</th>
                <th>Data dodania</th>
            </tr>
            <tbody>
                <?php
                    $pathToFiles = "./notes/";
                    $directory = opendir( $pathToFiles );
                    $count = 1;
                    $fileTable = [];
                    
                    while ( $file = readdir( $directory ) ) {
                        if ( is_file( $pathToFiles . $file ) ) {
                            $basename = basename( $file, ".txt" );
                            $mtime = filemtime( $pathToFiles . $file );
                            $mtimestr = date( "j F Y, H:i:s", $mtime );
                            
                            $temp['name'] = $basename;
                            $temp['time'] = $mtimestr;
                            $temp['path'] = $pathToFiles . $file;
                            $fileTable[$count] = $temp;
                            $count++;
                        }
                    }
                    
                    foreach ( $fileTable as $index => $entry ) {
                    $name = $entry['name'];
                    $time = $entry['time'];
                    $path = $entry['path'];
                    echo "<tr><td>$name</td>";
                    echo "<td>$time";
                    echo "<td><a href='edytuj_wpis.php?file=$path'>Edytuj</a>";
                    echo "<td><a href='usun_wpis.php?akcja=usun_plik&file=$path'>Usuń plik</a>";
                    }
                ?>
            </tbody>
        </table>
        <br/>
        <input type="button" name="nowa" value="Dodaj nową notatkę" onclick="window.location.href='dodaj_wpis.php'"/>
        <input type="button" name="powrot" value="Strona główna" onclick="window.location.href='index.php'"/>
    </body>
</html>
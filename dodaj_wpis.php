<html>
    <head>
        <meta charset="UTF-8">
        <title>Administracja</title>
    </head>
    <body>
        
    <form name="zapiszpliki" method="post" action="">
        Treść:
        <br/><textarea name="notatka" rows="20" cols=70"></textarea>
        <br/>
        <input type="submit" name="dodaj" value="Dodaj wpis"/>
        <input type="button" name="powrot" value="Powrót" onclick="window.location.href='administracja.php'"/>
    </form>

    <?php
        $path = './notes/';
        $directory = opendir($path);
        date_default_timezone_set('Europe/Warsaw');
        $dataig = date("Y-m-d h.i ");

        if (isset($_POST)) {
            if ($_POST["dodaj"]) {
                if (!file_exists($path.$dataig . ".txt")) {
                    $file = tmpfile();
                }
                $file = fopen($path.$dataig . ".txt", "w");

                $text = $_POST["notatka"];
                file_put_contents($path.$dataig . ".txt", $text);
                fclose($file);
                header("Location: ./administracja.php");
            }
        }
    ?>   
        Dodaj obrazek:
        <form enctype="multipart/form-data" action="nowy_obrazek.php" method="POST">
            <input name="plik" type="file">
            <input type="submit" value="Wstaw">
        </form>
        
        Wybierz obrazek:
        <table>
            <tr>
                <th>Obrazek</th>
                <th>Nazwa pliku</th>
                <th>Data</th>
                <th>Plik</th>
                <th>(Skopiuj jeśli chcesz dodać obrazek do notatki)/<th>

            </tr>
            <tbody>
                <?php
                    $pathToFiles = "./pictures/";
                    $directory = opendir($pathToFiles);
                    $count = 0;
                    $fileTable = [];
                    $temp = [];

                    while ($file = readdir($directory)) {
                        if (is_file($pathToFiles.$file)) {
                            $basename = basename($file, ".jpg");
                            $mtime = filemtime($pathToFiles.$file);
                            $mtimestr = date("j F Y, H:i:s", $mtime);
                            $temp = [];
                            $temp['picture'] = $pathToFiles.$file;
                            $temp['basename'] = $basename;
                            $temp['time'] = $mtimestr;
  
                            $count++;
                            $fileTable[$count] = $temp;
                            
                            foreach ( $fileTable as $index => $entry ) {
                                $picture = $entry['picture'];
                            $name = $entry['basename'];
                            $time = $entry['time'];

                            echo "<tr><td><img src='$picture' height=60 width=60>";
                            echo "<td>$name</td>";
                            echo "<td>$time</td>";
                            
                            echo "<td><a href='usun_plik.php?akcja=usun_plik&file=$pathToFiles$file'>Usuń plik</a>";
                            echo "<td><textarea cols='70'><img src='$pathToFiles$file' height=120 width=120><br/></textarea>";
                            }
                        }
                    }
                ?>
            </tbody>
        </table>
        
    </body>
</html>